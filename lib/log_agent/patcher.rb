module LogAgent::Patcher
  module_function

  def patch
    patch_rails if defined?(Rails)
    patch_rake if defined?(Rake)
    patch_sinatra if defined?(Sinatra)
    patch_active_record if defined?(ActiveRecord)
  end

  def patch_sinatra
    Sinatra::Application.use La::Rack::Logger
    Sinatra::Application.use La::Rack::ClientId
    Sinatra::Application.use La::Rack::App
  end

  def patch_rails
    La.log_file = "log/#{Rails.env}.log.json"

    Class.new(Rails::Railtie) {
      initializer "setup log_agent" do
        Rails.application.config.middleware.use La::Rack::Logger
        Rails.application.config.middleware.use La::Rack::ClientId
        Rails.application.config.middleware.use La::Rack::App
      end
    }
  end

  def patch_rake
    La.spy Rake::Task, :invoke, stream: true do |inst, orig, *args|
      La.set type: 'rake', name: inst.name, args: args
      orig.call
    end
  end

  def patch_active_record
    La.spy ActiveRecord::ConnectionAdapters::AbstractAdapter, :log do |inst, orig, sql, name='SQL', *rest|
      La.quiet! if name == 'SCHEMA' || La.root.params[:type] != 'http'
      La.set type: 'sql', query: sql
      orig.call
    end
  end
end
