require 'logger'
require 'json'
require 'securerandom'
require 'fileutils'

class LogAgent::Event
  attr_reader :parent
  attr_reader :children
  attr_reader :params

  def initialize(parent, stream: false)
    @parent = parent
    @stream = root? ? stream : root.stream?
    @children = []
    @quiet = false
    @params = {}

    parent.children << self if parent

    params = {time: Time.now.to_f}
    if stream?
      params[:id] = SecureRandom.uuid
      params[:parent] = parent.params[:id] if parent
    end

    @delayed_params = params.keys

    set params
  end

  def root
    event = self
    event = event.parent unless event.root?
    event
  end

  def root?
    @parent.nil?
  end

  def quiet!
    @quiet = true
  end

  def quiet?
    @quiet || (parent && parent.quiet?)
  end

  def stream?
    @stream
  end

  def set(params)
    @params.merge! params
    if stream?
      if @delayed_params.nil?
        log params
      elsif @delayed_params != params.keys
        @delayed_params = nil
        log @params
      end
    end
  end

  def safe_set(&block)
    begin
      set block.call
    rescue => e
      La.log_error e
    end
  end

  def record_duration
    set duration: Time.now.to_f - @params[:time]
  end

  def flush
    unless stream? || quiet?
      merge_event_params!
      log params
    end
  end

  def merge_event_params!
    unless children.empty?
      params[:events] = children
        .reject(&:quiet?)
        .each(&:merge_event_params!)
        .map(&:params)
    end
  end

  def log(params)
    unless quiet?
      params[:id] = @params[:id] if @params[:id]
      logger << JSON.dump(params) + "\n"
    end
  rescue => e
    logger << JSON.dump(type: "error", time: Time.now.to_f, error: e.inspect, error_backtrace: e.backtrace) + "\n"
  end

  def logger
    @@logger ||= begin
      File.write(La.log_file, "#{Time.now.to_f}\n") unless File.exists?(La.log_file)
      Logger.new(La.log_file)
    end
  end
end
