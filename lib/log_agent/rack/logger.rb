require 'set'

class LogAgent::Rack::Logger
  SLICE_KEYS_BEFORE = Set.new([
    'REMOTE_ADDR',
    'REQUEST_METHOD',
    'REQUEST_URI',
    'SERVER_NAME',
    'SERVER_PORT',
  ])
  SLICE_KEYS_AFTER = [
    'wg_clid',
    'rack.request.query_hash',
    'rack.request.form_hash',
    'sinatra.route',
    'action_dispatch.request.parameters',
    'action_dispatch.request.unsigned_session_cookie',
  ]

  def initialize(app)
    @app = app
  end

  def call(env)
    La.run_event {
      La.set type: 'http'
      La.safe_set { request_params_before(env) }

      begin
        resp = @app.call(env)
        La.safe_set { response_params(resp) }
        resp
      rescue => e
        La.set http_code: 500
        raise
      ensure
        La.safe_set { request_params_after(env) }
      end
    }
  end

  private

  def request_params_before(env)
    env.map {|key, value|
      if key.start_with?('HTTP_') || SLICE_KEYS_BEFORE.include?(key)
        [key, value.dup.force_encoding('utf-8')]
      end
    }.compact.to_h
  end

  def request_params_after(env)
    result = {}
    SLICE_KEYS_AFTER.each {|key|
      if env.key?(key)
        value = env[key]
        value = strip_params(env[key]) if value.kind_of?(Hash)
        result[key] = value
      end
    }
    result
  end

  def response_params(resp)
    code, headers = resp
    {
      http_code: code,
      repsonse_http_content_length: headers['Content-Length'],
    }
  end

  def strip_params(object)
    if object.kind_of?(Hash)
      object.map {|key, value|
        value = if key.to_s.include?('password')
          '<stripped>'
        elsif value.kind_of?(Hash) && value[:head]
          {head: value[:head]}
        else
          strip_params(value)
        end

        [key, value]
      }.to_h
    elsif object.kind_of?(Array)
      object.map {|item| strip_params(item)}
    else
      object
    end
  end
end
