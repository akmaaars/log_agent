class LogAgent::Rack::ClientId
  def initialize(app)
    @app = app
  end

  def call(env)
    resp = @app.call(env)

    if env['HTTP_COOKIE'] && (match = env['HTTP_COOKIE'].match(/wg_clid=([\d\.]+)/))
      env['wg_clid'] = match[1]
      resp
    else
      env['wg_clid'] = Time.now.to_f
      resp_obj = Rack::Response.new(resp[2], resp[0], resp[1])
      resp_obj.set_cookie 'wg_clid',
        expires: (Date.today + 3650).to_time,
        value: env['wg_clid']

      resp_obj.to_a
    end
  end
end
