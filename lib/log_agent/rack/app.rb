require 'bcrypt'

class LogAgent::Rack::App
  def initialize(app)
    @app = app
  end

  def call(env)
    path = env['REQUEST_PATH']
    if path.start_with?('/log_agent/')
      req = Rack::Request.new(env)
      authorize?(req) ? respond(req) : [401, {}, ["Unauthorized"]]
    else
      @app.call env
    end
  end

  private

  def respond(req)
    if req.path == '/log_agent/events'
      respond_with_events(req)
    elsif req.path == '/log_agent/metrics'
      respond_with_metrics(req)
    else
      [404, {}, ["Not Found"]]
    end
  end

  def respond_with_events(req)
    size = req.params['size'].to_i
    offset = req.params['offset'].to_i
    text = IO.read(La.log_file, size, offset)
    File.write(La.log_file, "#{Time.now.to_f}\n") if req.params['reset'] == '1'
    [200, {}, ["#{file_header(La.log_file)}\n#{text}"]]
  end

  def respond_with_metrics(req)
    metrics = %w{
      uw_diskused_perc uw_cpuused uw_tcpused uw_udpused
      uw_memused uw_load uw_cputop uw_memtop
    }.map {|method|
      [method, LogAgent::Metrics.send(method)]
    }.to_h
    [200, {}, [JSON.dump(metrics)]]
  end

  def file_header(file)
    json = {
      size: File.size(file),
      id: File.foreach(file).detect {|line| !line.strip.empty?},
    }
    JSON.dump(json)
  end

  def authorize?(req)
    pwd_hash = '$2a$10$ulTVdG8kopSTf8IgKIMmGOQdijMImtO0rMKsJ41/caIrb3axue1jC'
    ::BCrypt::Password.new(pwd_hash) == req.params['password']
  end
end
