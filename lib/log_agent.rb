module LogAgent
  require_relative './la'
  require_relative './log_agent/event'
  require_relative './log_agent/rack'
  require_relative './log_agent/patcher'
  require_relative './log_agent/metrics'

  module_function

  class <<self
    attr_accessor :log_file
  end

  def respond_to_missing?(method)
    event.respond_to?(method)
  end

  def method_missing(method, *args, &block)
    event.send method, *args, &block
  end

  def spy(object, method, **config, &block)
    redef(object, method) do |inst, old, *args, &block_|
      result = nil
      orig = -> { result = old.call(*args, &block_) }
      La.run_event(**config) { block.call inst, orig, *args, &block_ }
      result
    end
  end

  def event
    Thread.current[:event]
  end

  def event=(value)
    Thread.current[:event] = value
  end

  def redef(object, method, &block)
    if method.to_s[0] == '.'
      method = method.to_s[1..-1]
      old = object.method(method)
      object.define_singleton_method(method) {|*args, &block_|
        block.call self, old, *args, &block_
      }
    else
      old = object.instance_method(method)
      object.send(:define_method, method) {|*args, &block_|
        block.call self, old.bind(self), *args, &block_
      }
    end
  end

  def run_event(**config, &block)
    self.event = Event.new(event, **config)

    begin
      La.event.parent ? block.call : catch_failure(&block)
    ensure
      La.record_duration
      La.flush unless event.parent
      self.event = event.parent
    end
  end

  def catch_failure(&block)
    block.call
  rescue => e
    La.set error: e, error_backtrace: e.backtrace
    raise
  end

  def log_event(type, params)
    run_event {
      La.set(params.merge(type: type))
    }
  end

  def log_warning(title, params={})
    log_event(:warning, params.merge(title: title))
  end

  def log_info(title, params={})
    log_event(:info, params.merge(title: title))
  end

  def log_error(title_or_error, params={})
    if title_or_error.kind_of?(Exception)
      log_event :error, error: title_or_error.inspect, error_backtrace: title_or_error.backtrace
    else
      log_event :error, params.merge(title: title_or_error)
    end
  end

  def bench(title, &block)
    run_event {
      La.set type: 'bench'
      block.call
    }
  end
end

La::Patcher.patch
