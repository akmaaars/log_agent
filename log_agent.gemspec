# -*- encoding: utf-8 -*-
# stub: log_agent 0.7.8 ruby lib

Gem::Specification.new do |s|
  s.name = "log_agent".freeze
  s.version = "0.7.8"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["stenink".freeze]
  s.date = "2017-12-13"
  s.description = "".freeze
  s.email = ["stenink@yandex.ru".freeze]
  s.homepage = "".freeze
  s.rubygems_version = "2.6.14".freeze
  s.summary = "".freeze

  s.installed_by_version = "2.6.14" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<bcrypt>.freeze, ["~> 3.1"])
    else
      s.add_dependency(%q<bcrypt>.freeze, ["~> 3.1"])
    end
  else
    s.add_dependency(%q<bcrypt>.freeze, ["~> 3.1"])
  end
end
